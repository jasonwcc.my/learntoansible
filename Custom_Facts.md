{\rtf1\ansi\ansicpg1252\deff0\nouicompat{\fonttbl{\f0\fnil\fcharset0 Calibri;}}
{\colortbl ;\red200\green20\blue201;\red0\green0\blue0;\red193\green101\blue28;\red46\green174\blue187;\red180\green36\blue25;}
{\*\generator Riched20 10.0.19041}\viewkind4\uc1 
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\f0\fs22\lang9 ### Intro : \par
- Create additional information / parameters on managed hosts (not on master )\par
- Managed host admin can provide additional configuration information for controler to use\par
- Case use: What database service or web server flavor to use, or rules to add, selinux fcontent and many more\par
- file should be created in /etc/ansible/facts.d. If desired, set fact_path to other location with setup module.\par
- Facts can be dynamic or static, Static fact must not be executable . Dynamic fact must be executable\par
- Static can be in INI or JSON format. Dynamic can only use JSON format\par
- Cannot use YAML format at all\par
\par
\par
### EXAMPLE 1:\par
- create a custom fact file in node3\par
\par
```\par
vi /etc/ansible/facts.d/users.fact\par
---\par
[users]\par
user1 = joe\par
user2 = peter\par
\par
[packages]\par
web_pkg = httpd\par
db_pkg = mariadb-server\par
...\par
```\par
\par
### EXAMPLE 2:\par
- create custom fact file in specific node - servera\par
\par
```\par
vi files/custom.fact\par
---\par
[users]\par
user1 = joe\par
user2 = peter\par
\par
[packages]\par
web_pkg = httpd\par
db_pkg = mariadb-server\par
...\par
```\par
\par
- Back to controller, run following to query facts\par
```\par
ansible servera -m setup | grep -i ansible_local -A10\par
or\par
ansible servera -m setup -a "filter=ansible_local"\par
```\par
\par
\par
### EXAMPLE 3: Create playbook to display the custom fact from servera\par

\pard\sl240\slmult1 ```\par
---\par
- name: Install package specify by remote host\par
  hosts: servera\par
\par
  tasks:\par
  - debug:\par
      msg: >\par
       Installing \{\{ ansible_facts.ansible_local.custom.packages \}\}\par
       packages on \{\{ ansible_facts.fqdn \}\}\par
...\par

\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803 ```\par
\par
### EXAMPLE 4: Create playbook to really install package using fact from servera\par
```\par
---\par
- name: Install package specify by remote host\par
  hosts: servera\par
\par
  tasks:\par
  - yum:\par
      name: "\{\{ item \}\}"\par
    loop:\par
    - "\{\{ ansible_facts.ansible_local.custom.packages.web_package \}\}"\par
    - "\{\{ ansible_facts.ansible_local.custom.packages.db_package \}\}"\par
...\par
```\par
\par
\par
- If prefer can use control to push facts to managed host and ask remote admin to adjust the variable to their preference\par
```\par
vi create-facts.yml\par

\pard\tx560\tx1120\tx1680\tx2240\tx2800\tx3360\tx3920\tx4480\tx5040\tx5600\tx6160\tx6720\cf1 ---\cf2\par
\cf3 - \cf4 name\cf1 :\cf2  Create custom fact\par
  \cf4 hosts\cf1 :\cf2  all\par
  \cf4 become\cf1 :\cf2  yes\par
\par
  \cf4 tasks\cf1 :\cf2\par
  \cf3 - \cf4 name\cf1 :\cf2  Create dir first\par
    \cf4 file\cf1 :\cf2\par
      \cf4 path\cf1 :\cf2  \cf5 "/etc/ansible/facts.d"\cf2\par
      \cf4 state\cf1 :\cf2  directory\par
\par
  \cf3 - \cf4 name\cf1 :\cf2  Add custom fact file\par
    \cf4 copy\cf1 :\cf2\par
      \cf4 dest\cf1 :\cf2  /etc/ansible/facts.d/custom.fact\par
      \cf4 content\cf1 :\cf2  |\par
              \cf1 [\cf2 groups\cf1 ]\cf2\par
              group1 = salesg\par
              group2 = hrg\par
\par
              \cf1 [\cf2 users\cf1 ]\cf2\par
              user1 = joe\par
              user2 = peter\par
\par
              \cf1 [\cf2 packages\cf1 ]\cf2\par
              web_pkg = httpd\par
              db_pkg = mariadb-server\par
      \cf4 mode\cf1 :\cf2  "0600"\par
\par
  \cf3 - \cf4 name\cf1 :\cf2  Re-run setup\par
    \cf4 setup\cf1 :\cf2  \cf5 ~\cf2\par
\par
  \cf3 - \cf4 name\cf1 :\cf2\par
    \cf4 debug\cf1 :\cf2\par
      \cf4 var\cf1 :\cf2   ansible_local.custom.users.user1\par
\cf1 ...\cf2    \par

\pard\fi-11\li563\tx532\tx560\tx1120\tx1680\tx2240\tx2800\tx3360\tx3920\tx4480\tx5040\tx5600\tx6160\tx6720\cf5\par

\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\cf0\par
ansible-playbook facts.yml\par
\par
ansible all -m setup -a filter=ansible_local\par
```\par
\par
- Create a playbook to collect above custom facts\par
```\par
vi get-facts.yml\par

\pard\tx560\tx1120\tx1680\tx2240\tx2800\tx3360\tx3920\tx4480\tx5040\tx5600\tx6160\tx6720\cf1 ---\cf2\par
\cf3 - \cf4 name\cf1 :\cf2  Get custom fact\par
  \cf4 hosts\cf1 :\cf2  node1\par
  \cf4 become\cf1 :\cf2  yes\par
\par
  \cf4 tasks\cf1 :\cf2\par
  \cf3 - \cf4 name\cf1 :\cf2\par
    \cf4 debug\cf1 :\cf2\par
      \cf4 msg\cf1 :\cf2   \cf5 "\{\{ item.key \}\}"\par
\cf2     \cf4 with_dict\cf1 :\cf2  \cf5 "\{\{ ansible_local.custom \}\}"\cf2\par
...\par

\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\cf0 ansible-playbook get-facts.yml\cf2\par
\cf0\par
\par
### EXAMPLE 3:\par
- create manually getinfo.fact in node2\par
```\par
vi /etc/ansible/facts.d/getinfo.fact\par
\tab #!/bin/bash\par
\tab echo ['general']\par
\tab echo pwd = $PWD\par
\tab echo vg = researchvg\par
``\par
\par
- From control node run this\par
```\par
ansible node2 -m setup -a "ansible_local"\par
```\par
\par
\par
\cf5\par
}
 