### Intro : Populate /etc/hosts on all managed nodes
- We will collect ip, hostname, and fqdn from each managed hosts.
- With that we will add it into /etc/hosts of each managed hosts.

### The Steps...
- First create the j2 template file
```
vi populate_etc_hosts.j2
{% for host in groups['allservers'] %}
{{ hostvars[host].ansible_facts.default_ipv4.address }}  {{ hostvars[host].ansible_facts.hostname }} {{ hostvars[host].ansible_fqdn }} {{ hostvars[host].inventory_hostname }}
{% endfor %}
```

- Next create the playbook to process j2 template into individual managed hosts's /etc/hosts file
```
vi populate_etc_hosts.yml
---
- name: Populate /etc/hosts using template module
  hosts: all

  tasks:
  - template:
      src: populate_etc_hosts.j2
      dest: /etc/hosts2
...
```
- Runs it
```
ansible-playbook populate_etc_hosts.yml
```

- Verifies /etc/hosts content on each managed hosts
```
ssh 192.168.32.183 cat /etc/hosts
192.168.32.183  r8s3 r8s3.example.com servera
192.168.32.184  r8s4 r8s4.example.com serverb
192.168.32.201  o8s1 o8s1.example.com serverc

ssh 192.168.32.184 cat /etc/hosts
192.168.32.183  r8s3 r8s3.example.com servera
192.168.32.184  r8s4 r8s4.example.com serverb
192.168.32.201  o8s1 o8s1.example.com serverc
```
