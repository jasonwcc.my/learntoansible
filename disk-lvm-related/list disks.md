# Showing new and old method in discovering typ eof disks
- New code
```
---
- name: do something
  hosts: node4
  become: yes

  tasks:
  - name: Discover scsi or sata disks only
    set_fact:
      device_name: "{{ item }}"
      no_log: false
    with_dict: "{{ ansible_devices }}"
    #when: "item.value.host.startswith('SATA')"
    when: "item.key.startswith('sd')"

  - name: show all values for selected device name
    debug: 
      var: item
    with_dict: "{{ device_name }}"

  - name: "show only {{ device_name }}"
    debug: var=device_name
...

- Old Code
```
---
- name: do something
  hosts: node4
  become: yes

  tasks:
  - name: Discover scsi or sata disks only
    set_fact:
      device_name: "{{ item.key }}"
      no_log: false
    with_dict: "{{ ansible_devices }}"
    #when: "item.value.host.startswith('SATA')"
    when: "item.key.startswith('sd')"

  - name: show all values for selected device name
    debug: 
      var: item
    with_dict: "{{ device_name }}"

  - name: show only device name
    debug: var=device_name
...


