- Example 1 :
```
---
- name: Task LVM
  hosts: node1,node2
  become: yes

  vars:
    vg: "researchvg"
    min: "0.5"
    min_1g: "1"
    lv: "db_vol1"

  tasks:
  - name: Look for {{ vg }} method1
    command:
       cmd: vgs {{ vg }}
    register: vgs_result
    failed_when: "'FAILED' in vgs_result.stderr"

  - name: If {{ vg }} not found, display custom error
    debug:
      msg: "{{ vg }} not found in {{ inventory_hostname }}"
    when: vgs_result.stderr != ""
...
```


- Example 2 :
```
---
- name: Task LVM
  hosts: node1,node2
  become: yes

  vars:
    vg: "researchvg"
    min: "0.5"
    min_1g: "1"
    lv: "db_vol1"

  tasks:
  - name: Look for {{ vg }} method2
    debug:
      msg: "{{ vg }} found in {{ inventory_hostname }}"
    when: item.key|string == "researchvg"
    with_dict: "{{ ansible_lvm.vgs }}"
...
```
