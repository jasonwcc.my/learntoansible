```
---
- name: Get list of mount points
  hosts: node2
  become: yes

  tasks:
  - debug:
     msg: "{{ ansible_mounts }}"
...
```
- The result
```
ok: [node2] => {
    "msg": [
        {
            "block_available": 3323510,
            "block_size": 4096,
            "block_total": 4452864,
            "block_used": 1129354,
            "device": "/dev/mapper/rhel-root",
            "fstype": "xfs",
            "inode_available": 8789950,
            "inode_total": 8910848,
            "inode_used": 120898,
            "mount": "/",
            "options": "rw,seclabel,relatime,attr2,inode64,noquota",
            "size_available": 13613096960,
            "size_total": 18238930944,
            "uuid": "0549459c-89c0-42e2-a838-a2890b354543"
        },
        {
            "block_available": 200984,
            "block_size": 4096,
            "block_total": 259584,
            "block_used": 58600,
            "device": "/dev/nvme0n1p1",
            "fstype": "xfs",
            "inode_available": 523987,
            "inode_total": 524288,
            "inode_used": 301,
            "mount": "/boot",
            "options": "rw,seclabel,relatime,attr2,inode64,noquota",
            "size_available": 823230464,
            "size_total": 1063256064,
            "uuid": "f96a48b6-f836-494a-bb78-5d12234be8be"
        }
    ]
}
```

- Try another
```
---
  tasks:
  - debug:
     msg: "{{ ansible_mounts }} | map(attribute='mount') | join(',') }}"
...
```

- Result
```
ok: [node2] => {
    "msg": "[{'mount': '/', 'device': '/dev/mapper/rhel-root', 'fstype': 'xfs', 'options': 'rw,seclabel,relatime,attr2,inode64,noquota', 'size_total': 18238930944, 'size_available': 13613117440, 'block_size': 4096, 'block_total': 4452864, 'block_available': 3323515, 'block_used': 1129349, 'inode_total': 8910848, 'inode_available': 8789950, 'inode_used': 120898, 'uuid': '0549459c-89c0-42e2-a838-a2890b354543'}, {'mount': '/boot', 'device': '/dev/nvme0n1p1', 'fstype': 'xfs', 'options': 'rw,seclabel,relatime,attr2,inode64,noquota', 'size_total': 1063256064, 'size_available': 823230464, 'block_size': 4096, 'block_total': 259584, 'block_available': 200984, 'block_used': 58600, 'inode_total': 524288, 'inode_available': 523987, 'inode_used': 301, 'uuid': 'f96a48b6-f836-494a-bb78-5d12234be8be'}] | map(attribute='mount') | join(',') }}"
}
```

- Convert ansible_mounts into items list
```
---
  tasks:
  - debug:
     msg: "{{ item | dict2items }}"
    with_dict: "{{ ansible_mounts }}"
    when: item.key == "inode_used"
    #when: item.value =="/mnt/database"
...
```
