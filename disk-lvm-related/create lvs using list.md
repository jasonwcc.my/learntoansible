### Dynamic Partitioning, configure LV, configure FS, and then mount them
- First define variables with respective VGs and size

```
vi lvs-list.yml
---
lvols:
 - lvname: db_lv1
   vg: researchvg
   size: 100m
 - lvname: db_lv2
   vg: researchvg
   size: 200m
 - lvname: ftp_lv1
   vg: rhel
   size: 100m
...
```

- Nxt we use above information to  configure new LV
```
vi create.lvs.yml
---
- name: Create lvs
  become: yes
  hosts: all

  # Find researchvg and ignore task if not found
  tasks: Find researchvg
  - name: 
    shell: "vgs | grep researchvg"
    register: result
    ignore_errors: true

  # Display error when vg not found
  - name: Display error 
    debug:
     msg: "Researchvg not found in {{ inventory_hostname }}"
    when: result.rc != 0

  - include_vars: lvs-list.yml
  - lvol:
       lv: "{{ item.lvname }}"
       size: "{{ item.size }}"
       vg: "{{ item.vg }}"
    with_items: "{{ lvols }}"
    when: "result.rc == 0 and item.vg == 'researchvg'"
...
```

- Play and see ifs is okay
```
ansible-playbook create.lvs.yml                                  
```