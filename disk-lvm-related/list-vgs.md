- List all configured VGS and their free space
```
---
- name: List all vgs
  hosts: all
  become: yes

  tasks:
  - debug:
     msg: >
        "echo {{ item.key }} vg  
        has free capacity of {{ item.value.free_g }}" GiB
    with_dict: "{{ ansible_lvm.vgs }}"
...
```

- List specific VGS
```
---
- name: List vgs
  hosts: all
  become: yes

  tasks:
  - debug:
     msg: >
        "echo {{ item.key }} vg  
        has free capacity of {{ item.value.free_g }}" GiB
    with_dict: "{{ ansible_lvm.vgs }}"
    when: not item.key == "rhel" and not item.key == "cl"
```

- The result
```
TASK [debug] ********************************************************************
ok: [node2] => (item={'key': 'researchvg', 'value': {'size_g': '3.00', 'free_g': '3.00', 'num_lvs': '0', 'num_pvs': '1'}}) => {
    "msg": "\"echo researchvg vg has 3.00\"\n"
}
ok: [node2] => (item={'key': 'rhel', 'value': {'size_g': '19.00', 'free_g': '0', 'num_lvs': '2', 'num_pvs': '1'}}) => {
    "msg": "\"echo rhel vg has 0\"\n"
}
ok: [node3] => (item={'key': 'cl', 'value': {'size_g': '19.00', 'free_g': '0', 'num_lvs': '2', 'num_pvs': '1'}}) => {
    "msg": "\"echo cl vg has 0\"\n"
}
ok: [node3] => (item={'key': 'researchvg', 'value': {'size_g': '1.00', 'free_g': '1.00', 'num_lvs': '0', 'num_pvs': '1'}}) => {
    "msg": "\"echo researchvg vg has 1.00\"\n"
}
ok: [node3] => (item={'key': 'vg_database', 'value': {'size_g': '0.78', 'free_g': '0.28', 'num_lvs': '1', 'num_pvs': '1'}}) => {
    "msg": "\"echo vg_database vg has 0.28\"\n"
}
ok: [node1] => (item={'key': 'rhel', 'value': {'size_g': '19.00', 'free_g': '0', 'num_lvs': '2', 'num_pvs': '1'}}) => {
    "msg": "\"echo rhel vg has 0\"\n"
}
ok: [node4] => (item={'key': 'VGA', 'value': {'size_g': '0.97', 'free_g': '0.74', 'num_lvs': '2', 'num_pvs': '2'}}) => {
    "msg": "\"echo VGA vg has 0.74\"\n"
}
ok: [node4] => (item={'key': 'VGB', 'value': {'size_g': '0.97', 'free_g': '0.55', 'num_lvs': '2', 'num_pvs': '2'}}) => {
    "msg": "\"echo VGB vg has 0.55\"\n"
}
ok: [node4] => (item={'key': 'cl', 'value': {'size_g': '19.00', 'free_g': '0', 'num_lvs': '2', 'num_pvs': '1'}}) => {
    "msg": "\"echo cl vg has 0\"\n"
}
ok: [node4] => (item={'key': 'researchvg', 'value': {'size_g': '0.48', 'free_g': '0.48', 'num_lvs': '0', 'num_pvs': '1'}}) => {
    "msg": "\"echo researchvg vg has 0.48\"\n"
}
```