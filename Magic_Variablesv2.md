### Intro
Variables that is accessible even before fact_gathering

Commonly used
- hostvars
- groups
- group_names
- inventory_hostname

### Caveats:
If playbook runs only on specify host or group,
- you will have trouble getting magic variable from other groups. 
- See example below

### Example : Ad Hoc to reveal some facts on managed hosts
```
ansible all -m debug -a "msg={{ groups }}"
ansible all -m debug -a "msg='this is {{ inventory_hostname}} in {{ group_names }}'"
```


 ### Example : Simple playbook to reveal some facts on managed hosts

```
vi hostvars.yml
---
- name: Show msg with hostvars
  hosts: localhost
  become: yes

  tasks:
  - debug:
      msg:
       - "{{ hostvars['node1']['ansible_facts']['hostname'] }}"
       - "{{ hostvars['node1']['ansible_facts']['distribution'] }}"
       - "{{ hostvars['node1']['ansible_facts']['default_ipv4']['address'] }}"
...
ansible-playbook hostvars.yml
```

### Example  : Produce /tmp/information with respective hardware information on managed hosts

```
vi inventory
---
node1   ansible_host=192.168.165.141
node2   ansible_host=r8s2.example.com
node3   ansible_host=c8s1.example.com
node4   ansible_host=r8s4.example.com

[all]
node1
node2
node3
node4

[all:vars]
ansible_user=aaa
ansible_password=aaa
ansible_connection=ssh

[clients]
node2
node4

[haproxy]
node1

[webservers:children]
clients
haproxy
...
```

-  In case u want to look for other fact gather 
```
ansible localhost -m setup "filter=*processor*"
```

- Create the template with for and loop statements

```
...

vi information.j2
This lines appears only if fqdn is r8s3.example.com
{% if ansible_host == "r8s3.example.com" %}
   Hostname             : {{ ansible_facts.fqdn }}
   Total swap memory    : {{ ansible_memory_mb.swap.free }} MiB
   Number of core(s)    : {{ ansible_processor_cores }} 
{% endif %}

This line appears only on nodes that is member of webservers
{% if 'webservers' in group_names %}
   Hostname             : {{ ansible_facts.fqdn }}
   IP                   : {{ ansible_facts.default_ipv4.address }}
   Total Memory         : {{ ansible_memtotal_mb }} MiB
{% endif %}

This lines appears only if inventory_hostname is serverd
{% if inventory_hostname == "serverd" %}
   Hostname             : {{ ansible_facts.fqdn }}
   /dev/sda             : {{ ansible_devices.sda.size | default("NONE") }}
{% endif %}

This lines appears on all nodes
{% for host in groups['allservers'] %}
   Hostname             : {{ hostvars[host]['ansible_facts']['hostname'] }}
   Inventory Hostname   : {{ hostvars[host]['inventory_hostname'] }}
{% endfor %}
...
```


- Create playbook to template from previous jinja template
```
vi playbook.yml
---
- name: Show msg with hostvars
  hosts: all
  become: yes

  tasks:
  - name : display msg
    template:
      src: information.j2
      dest: /tmp/information.txt
      owner: root
      group: root
      mode: '0666'

...

ansible-playbook playbook.yml

ansible all -m shell -a "cat /tmp/information.txt"
```
